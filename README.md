
# metamuffin.org

This repository contains sources for [metamuffin.org](https://metamuffin.org/)

## Licencing

This project is licenced under the third version of the GNU Affero Public Licence only, see [`COPYING`](./COPYING).

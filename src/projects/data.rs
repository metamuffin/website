use super::{Project, State::*, Status::*};

pub const PROJECTS: &'static [Project] = &[
    Project {
        name: "gnix",
        status: Maintained,
        description: "a stupid reverse proxy to replace nginx. serving the webpage you are viewing right now.",
        link: Some("https://metamuffin.org"),
        ..default()
    },
    Project {
        name: "keks-meet",
        status: Maintained,
        description: "a simple secure web conferencing application.",
        link: Some("https://meet.metamuffin.org"),
        ..default()
    },
    Project {
        name: "hurrycurry",
        status: Maintained,
        description: "A cooperative video game about cooking. Featuring multiplayer, 3d graphics and more.",
        link: Some("https://hurrycurry.metamuffin.org"),
        repo_link: Some("https://codeberg.org/hurrycurry/hurrycurry"),
        ..default()
    },
    Project {
        name: "weareserver",
        status: Developing,
        description: "server software (and experimental client) for a generic 3d multiplayer game with VoIP capabilities. Inspired by VRChat and Minetest.",
        ..default()
    },
    Project {
        name: "abrechenbarkeit",
        status: Maintained,
        description: "a simple trust-based ledger implemented as a Lua CGI web application with a CSV database",
        ..default()
    },
    Project {
        name: "jellything",
        status: Paused(Working),
        description: "(paused until accumulated technical dept is fixed) media streaming solution (similiar to jellyfin). supports on-the-fly remuxing and federated content.",
        ..default()
    },
    Project {
        name: "pfadfinder",
        status: Paused(Working),
        description: "parallel anytime A* for openstreetmap with custom frontend and integration into the existing one.",
        ..default()
    },
    Project {
        name: "video-codec-experiments",
        status: Paused(Working),
        description: "a few attempts of creating my own video codecs, making use of motion compensation, dct, quantization and more. ",
        ..default()
    },
    Project {
        name: "pixelflut tools",
        status: Paused(Working),
        description: "the programs I hacked together when playing pixelflut. includes GPU-acceleration for updating the canvas with minimal bandwidth usage.",
        ..default()
    },
    Project {
        name: "attocc",
        status: Paused(Unfinished),
        description: "minimal C compiler in C",
        ..default()
    },
    Project {
        name: "trash-proxy",
        status: Maintained,
        description: "A minecraft reverse proxy for offline authentification.",
        ..default()
    },
    Project {
        name: "voxelwagen",
        status: Paused(Unfinished),
        description: "voxel game engine made from scratch; made to host an automation game.",
        repo_link: Some("https://codeberg.org/voxelwagen/voxelwagen"),
        ..default()
    },
    Project {
        name: "reCYCLING",
        status: Developing,
        description: "A fun CAPTCHA solution",
        ..default()
    },
    Project {
        name: "keckup",
        status: Maintained,
        description: "Backup utility that traverses the filesystem while applying .gitignore and .backupignore files",
        ..default()
    },
    Project {
        name: "ebml-struct",
        status: Maintained,
        description: "EBML parser and generator library with integrated Matroska types implemented in Rust without proc macros",
        ..default()
    },
];
pub const EXTRA_PROJECTS: &'static [Project] = &[
    Project {
        name: "mond",
        status: Paused(Unfinished),
        description: "server monitoring software, no much code written so far",
        ..default()
    },
    Project {
        name: "infra",
        status: Paused(Working),
        description: "multi-system installation manager. allows to centrally manage installations with a single configuration in form of a rust DSL",
        ..default()
    },
    Project {
        name: "pbot",
        status: Developing,
        description: "Opinionated rust library for writing high-level chatbots that concurrently run on multiple messengers.",
        ..default()
    },
    Project {
        name: "karlender",
        status: Paused(Working),
        description: " a personal calender suite consisting of a daemon, a graphical and a command-line user interface. supports very flexible condition based recurring tasks and automatic scheduling so you dont need to decide.",
        ..default()
    },
    Project {
        name: "bad-editor",
        status: Paused(Unfinished),
        description: "A really bad editor",
        ..default()
    },
    Project {
        name: "cosu",
        status: Paused(Unfinished),
        description: "a simple fast osu! client in c (not nearly a working client yet)",
        ..default()
    },
    Project {
        name: "sprakc",
        status: Abandoned(Working),
        description: "A compiler from c++ to sprak",
        ..default()
    },
    Project {
        name: "sprak-runtime",
        status: Abandoned(Working),
        description: "",
        ..default()
    },
    Project {
        name: "ehb-data",
        status: Abandoned(Working),
        description: "",
        ..default()
    },
    Project {
        name: "sgf",
        status: Unknown,
        description: "",
        ..default()
    },
    Project {
        name: "circuit-simulator",
        status: Unknown,
        description: "",
        ..default()
    },
    Project {
        name: "metabot",
        status: Paused(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "website",
        status: Maintained,
        description: "",
        ..default()
    },
    Project {
        name: "ld49",
        status: Abandoned(Unfinished),
        description: "Ludum dare 49",
        ..default()
    },
    Project {
        name: "bot-game",
        status: Unknown,
        description: "",
        ..default()
    },
    Project {
        name: "game-platform",
        status: Unknown,
        description: "",
        ..default()
    },
    Project {
        name: "simple-game-engine",
        status: Paused(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "voice-control",
        status: Paused(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "libreverse-old",
        status: Abandoned(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "golf-lang",
        status: Abandoned(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "mumble-music",
        status: Paused(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "muon",
        status: Paused(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "libresyncedvideoplayer",
        status: Unknown,
        description: "",
        ..default()
    },
    Project {
        name: "fireworks",
        status: Abandoned(Unfinished),
        description: "Fireworks with Three.js",
        ..default()
    },
    Project {
        name: "bj1",
        status: Unknown,
        description: "",
        ..default()
    },
    Project {
        name: "so-survey",
        status: Paused(Working),
        description: "Some code for processing the stackoverflow developer survey's results",
        ..default()
    },
    Project {
        name: "rubberkeys",
        status: Abandoned(Working),
        description: "",
        ..default()
    },
    Project {
        name: "lunar-lander",
        status: Abandoned(Working),
        description: "",
        ..default()
    },
    Project {
        name: "molecule-viewer",
        status: Paused(Working),
        description: "Web application to visualize layout of molecules",
        ..default()
    },
    Project {
        name: "oxelpot",
        status: Paused(Working),
        description: "text-board service using pure html on the client",
        ..default()
    },
    Project {
        name: "CUBE",
        status: Abandoned(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "godot-mumble-link",
        status: Paused(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "libreverse-server",
        status: Unknown,
        description: "2d multiplayer game engine server",
        ..default()
    },
    Project {
        name: "twclient",
        status: Paused(Unfinished),
        description: "A teeworlds client in rust, based on libtw2",
        ..default()
    },
    Project {
        name: "minetest_meta_uhc",
        status: Paused(Unfinished),
        description: "ultra hardcore mod for minetest with decreasing border size",
        ..default()
    },
    Project {
        name: "guitar-chords",
        status: Paused(Unfinished),
        description: "",
        ..default()
    },
    Project {
        name: "gnu-netcat",
        status: Paused(Working),
        description: "Mirror of the “GNU“ netcat utility (imported from CVS)",
        ..default()
    },
    Project {
        name: "stonk-game",
        status: Abandoned(Unfinished),
        description: "simple game to compete in trading stocks",
        ..default()
    },
    Project {
        name: "pw-patchbay",
        status: Paused(Working),
        description: "Tool to setup links for pipewire",
        ..default()
    },
    Project {
        name: "desterr",
        status: Maintained,
        description: "dead simple tool to distinguish stderr and stdout",
        ..default()
    },
    Project {
        name: "staticwiki",
        status: Paused(Working),
        description: "tools to convert MediaWiki XML dumps to static HTML pages",
        ..default()
    },
    Project {
        name: "trash-map",
        status: Paused(Working),
        description: "A minecraft map renderer",
        ..default()
    },
    Project {
        name: "acoustsearch",
        status: Paused(Unfinished),
        description: "Search the acoustid database locally.",
        ..default()
    },
    Project {
        name: "meta_shell",
        status: Paused(Unfinished),
        description: "Simple wayland shell only including a status dock.",
        ..default()
    },
    Project {
        name: "3d-frame-interpolation",
        status: Paused(Working),
        description: "",
        ..default()
    },
    Project {
        name: "librelambda",
        status: Paused(Working),
        description: "A lambda calculus \"solver\".",
        ..default()
    },
    Project {
        name: "automato",
        status: Paused(Unfinished),
        description: "A minimal, gridless automation game.",
        ..default()
    },
];

const fn default() -> Project {
    Project {
        name: "",
        status: super::Status::Unknown,
        description: "",
        link: None,
        repo_link: None,
    }
}

use self::data::{EXTRA_PROJECTS, PROJECTS};
use crate::layout::{DynScaffold, Scaffold};
use markup::Render;
use rocket::get;

pub mod data;

#[get("/projects")]
pub fn r_projects() -> DynScaffold<'static> {
    Scaffold {
        title: "projects".to_string(),
        content: markup::new! {
            p { "I am starting a lot of projects - this page lists some of them." }
            p {
                "Starting so many means, that most of then are not maintained or not even properly developed. "
                "Here is a quick reference to what I define the status of a project to be: "
            }
            ol {
                li { @Status::Planned.render() ": No code has been written yet." }
                li { @Status::Developing.render() ": Project is under active development." }
                li { @Status::Maintained.render() ": Project is in a working state and receives regular updates." }
                li { @Status::Paused(State::Working).render() @Status::Paused(State::Unfinished).render() ": Project has been discontinued for an unspecified amount of time, but might be resumed if i feel like it." }
                li { @Status::Abandoned(State::Working).render() @Status::Abandoned(State::Unfinished).render() ": Project has been discontinued and will likely not ever be continued." }
                li { @Status::Unknown.render() ": I have not bothered to write down the status" }
            }
            ul { @for p in PROJECTS { li { @p } } }
            details { summary { "Other not-so-important projects" }
                ul { @for p in EXTRA_PROJECTS { li { @p } } }
            }
        },
    }
}

#[derive(Debug, Clone, Copy)]
pub enum State {
    Working,
    Unfinished,
}

#[derive(Debug, Clone, Copy)]
pub enum Status {
    Unknown,
    Planned,
    Developing,
    Paused(State),
    Maintained,
    Abandoned(State),
}

markup::define! { Project(
    name: &'static str,
    status: Status,
    description: &'static str,
    link: Option<&'static str>,
    repo_link: Option<&'static str>
) {
    b { @name }
    @status.render()
    ": " @description
    " ("
    @if let Some(link) = link {
        a[href=link] "Project page"
        ", "
    }
    @let fallback = format!("https://codeberg.org/metamuffin/{name}");
    a[href=repo_link.unwrap_or(&fallback)] "Source code"
    ")"
}}

impl Status {
    fn ident(self) -> &'static str {
        match self {
            Status::Unknown => "unknown",
            Status::Planned => "planned",
            Status::Developing => "developing",
            Status::Maintained => "maintained",
            Status::Paused(State::Unfinished) => "paused-unfinished",
            Status::Paused(State::Working) => "paused-working",
            Status::Abandoned(State::Unfinished) => "abandoned-unfinished",
            Status::Abandoned(State::Working) => "abandoned-working",
        }
    }
    pub fn render(self) -> impl Render {
        markup::new! {
           div[class=format!("status status-{}", self.ident())] { @format!("{self:?}") }
        }
    }
}

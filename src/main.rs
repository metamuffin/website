/*
    This file is part of metamuffins website (https://codeberg.org/metamuffin/website)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin.org>
*/
#![feature(const_trait_impl)]
pub mod blog;
pub mod error;
pub mod layout;
pub mod pages;
pub mod projects;
pub mod source;
pub mod wellknown;

use blog::*;
use error::*;
use pages::*;
use projects::*;
use rocket::{catchers, fairing::AdHoc, fs::FileServer, http::Header, routes};
use source::*;
use wellknown::*;

#[tokio::main]
async fn main() {
    env_logger::init_from_env("LOG");
    let _ = rocket::build()
        .attach(AdHoc::on_response("set server header", |_req, res| {
            res.set_header(Header::new("server", "blub"));
            Box::pin(async {})
        }))
        .manage(prepare_source())
        .mount("/blog/assets", FileServer::from(ASSET_ROOT))
        .mount(
            "/",
            routes![
                r_root,
                r_about,
                r_contact,
                r_projects,
                r_pgp_key,
                r_source,
                r_blog,
                r_stuff,
                r_hello,
                r_favicon,
                r_blog_index,
                r_blog_article,
                r_blog_atom,
                r_style,
                r_wellknown_security,
                r_wellknown_matrix_server,
                r_wellknown_matrix_client,
                r_wellknown_flathub_verified,
            ],
        )
        .register("/", catchers![r_catch])
        .launch()
        .await
        .unwrap();
}

#[macro_export]
macro_rules! uri {
    ($kk:stmt) => {
        &rocket::uri!($kk).to_string()
    };
}

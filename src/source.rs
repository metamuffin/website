use crate::layout::{DynScaffold, Scaffold};
use rocket::{
    get,
    http::Header,
    response::{self, Responder},
    Request, State,
};
use std::time::{Duration, SystemTime, UNIX_EPOCH};

pub struct Reload<T>(pub T);

#[rocket::async_trait]
impl<'r, T: Responder<'r, 'static>> Responder<'r, 'static> for Reload<T> {
    fn respond_to(self, request: &'r Request<'_>) -> response::Result<'static> {
        let mut resp = self.0.respond_to(request);
        if let Ok(resp) = &mut resp {
            resp.set_header(Header::new("refresh", "0"));
        }
        resp
    }
}

const SOURCE_DIR: include_dir::Dir = include_dir::include_dir!("$CARGO_MANIFEST_DIR/src");

pub struct SourceWrap(Vec<String>);

pub fn prepare_source() -> SourceWrap {
    SourceWrap(
        SOURCE_DIR
            .find("**/*.rs")
            .unwrap()
            .map(|f| {
                format!(
                    "
======================================
    Contents of {:?}
======================================
{}
",
                    f.path(),
                    f.as_file().unwrap().contents_utf8().unwrap()
                )
            })
            .collect::<Vec<_>>()
            .join("\n")
            .split("\n")
            .map(String::from)
            .collect(),
    )
}

#[get("/source")]
pub async fn r_source(text: &State<SourceWrap>) -> Reload<DynScaffold> {
    let mspf = 100u128;

    let ts = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis();

    let frame = ts / mspf;
    let frame_off = ts % mspf;

    tokio::time::sleep(Duration::from_millis((mspf - frame_off) as u64)).await;

    let mut out = String::new();
    out += &format!(
        "About {} milliseconds have passed since midnight of the january the first in 1970.\n",
        ts
    );
    out += "------------------------------------------------------\n";
    for i in frame..frame + 30 {
        out += &text.0[(i % text.0.len() as u128) as usize];
        out += "\n";
    }
    Reload(Scaffold {
        title: "Source".to_string(),
        content: markup::new! { pre { @out } },
    })
}

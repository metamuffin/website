use std::fmt::Display;

use crate::layout::{DynScaffold, Scaffold};
use rocket::{
    catch,
    http::Status,
    response::{self, Responder},
    Request,
};

#[catch(default)]
pub fn r_catch<'a>(status: Status, _request: &Request) -> DynScaffold<'a> {
    Scaffold {
        title: "Error".to_string(),
        content: markup::new! {
            h2 { "Error" }
            p { @format!("{status}") }
        },
    }
}

pub type MyResult<T> = Result<T, MyError>;

#[derive(Debug)]
pub struct MyError(pub anyhow::Error);

impl<'r> Responder<'r, 'static> for MyError {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        Scaffold {
            title: "Error".to_string(),
            content: markup::new! {
                h2 { "An error occured. Nobody is sorry" }
                pre.error { @format!("{:?}", self.0) }
            },
        }
        .respond_to(req)
    }
}

impl Display for MyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}
// impl<T: std::error::Error> From<T> for MyError {
//     fn from(err: T) -> MyError {
//         MyError(anyhow::anyhow!("{err}"))
//     }
// }
impl From<std::io::Error> for MyError {
    fn from(err: std::io::Error) -> MyError {
        MyError(anyhow::anyhow!("{err}"))
    }
}
impl From<anyhow::Error> for MyError {
    fn from(err: anyhow::Error) -> MyError {
        MyError(err)
    }
}

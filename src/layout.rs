/*
This file is part of metamuffins website (https://codeberg.org/metamuffin/website)
which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
Copyright (C) 2023 metamuffin <metamuffin.org>
*/

use crate::blog::rocket_uri_macro_r_blog;
use crate::pages::{
    rocket_uri_macro_r_about, rocket_uri_macro_r_contact, rocket_uri_macro_r_pgp_key,
    rocket_uri_macro_r_stuff,
};
use crate::projects::rocket_uri_macro_r_projects;
use crate::source::rocket_uri_macro_r_source;
use crate::uri;
use markup::{raw, Render};
use rocket::{
    http::ContentType,
    response::{self, Responder},
    Request, Response,
};
use std::io::Cursor;

markup::define! {
    ScaffoldImpl<Main: Render>(
        title: String,
        main: Main,
        noimg: bool,
    ) {
        @markup::doctype()
        html[lang="en"] {
            head {
                title { @title " - " "metamuffin's website" }
                meta[name="viewport", content="width=device-width, initial-scale=1.0"];
                meta[name="description", content="metamuffin's personal website"]; // TODO
                link[rel="stylesheet", href="/style.css"];
            }
            body {
                @if !noimg { img[
                    src="https://s.metamuffin.org/avatar/default-512.webp",
                    alt="a muffin with purple glowing regions where a 3d vornoi function using chebychev distance exceeds some threshold",
                    align="left",
                    height=80,
                    width=80,
                    hspace=10,
                ]; }
                h1 { "metamuffin's personal website" }
                nav {
                    a[href=uri!(r_about())] { "About" } " "
                    a[href=uri!(r_blog())] { "Blog" } " "
                    a[href=uri!(r_projects())] { "Projects" } " "
                    a[href=uri!(r_contact())] { "Contact" } " "
                    a[href=uri!(r_stuff())] { "Stuff" } " "
                    a[href="https://codeberg.org/metamuffin"] { "Codeberg" } " "
                    a[href=uri!(r_pgp_key())] { "PGP-Key" } " "
                }
                hr;
                article { @main }
                hr;
                footer {
                    p {
                        "metamuffin's website; version " @include_str!("../.git/refs/heads/main")[0..10] "; "
                        "sources available on " a[href=uri!(r_source())] { "this page itself" }
                        " and on " a[href="https://codeberg.org/metamuffin/metamuffin-website"] { "codeberg" }
                    }
                }
                hr;
                @if !noimg {
                    details.ad { summary { "Advertisement by a first-party" }
                        iframe[loading="lazy", src="https://adservices.metamuffin.org/v1/embed?s=metamuffin.org", style="width:728px;height:90px;border:none;"] {}
                    }
                    details.ad { summary { "Advertisement by a third-party" }
                        iframe[loading="lazy", src="https://john.citrons.xyz/embed?ref=metamuffin.org", style="width:732px;height:94px;border:none;"] {}
                    }
                    script { @raw("document.querySelectorAll(\".ad\").forEach(e=>e.open=true) // evil js enables ads hehe") }
                }
            }
        }
    }
}

pub type DynScaffold<'a> = Scaffold<markup::DynRender<'a>>;

pub struct Scaffold<T> {
    pub title: String,
    pub content: T,
}

impl<'r, Main: Render> Responder<'r, 'static> for Scaffold<Main> {
    fn respond_to(self, _req: &'r Request<'_>) -> response::Result<'static> {
        let mut out = String::new();
        ScaffoldImpl {
            main: self.content,
            noimg: self.title == "Source",
            title: self.title,
        }
        .render(&mut out)
        .unwrap();

        Response::build()
            .header(ContentType::HTML)
            .streamed_body(Cursor::new(out))
            .ok()
    }
}

use rocket::{
    get,
    http::Header,
    response::{self, Responder},
    serde::json::{json, Value},
    Request,
};

pub struct Cors<T>(pub T);

#[rocket::async_trait]
impl<'r, T: Responder<'r, 'static>> Responder<'r, 'static> for Cors<T> {
    fn respond_to(self, request: &'r Request<'_>) -> response::Result<'static> {
        let mut resp = self.0.respond_to(request);
        if let Ok(resp) = &mut resp {
            resp.set_header(Header::new("access-control-allow-origin", "*"));
        }
        resp
    }
}

#[get("/.well-known/matrix/client")]
pub fn r_wellknown_matrix_client() -> Cors<Value> {
    Cors(json!({"m.homeserver": {"base_url": "https://matrix.metamuffin.org"}} ))
}

#[get("/.well-known/matrix/server")]
pub fn r_wellknown_matrix_server() -> Cors<Value> {
    Cors(json!({"m.server": "matrix.metamuffin.org:443"} ))
}

#[get("/.well-known/security.txt")]
pub fn r_wellknown_security() -> &'static str {
    include_str!("../assets/security.txt.asc")
}

#[get("/.well-known/org.flathub.VerifiedApps.txt")]
pub fn r_wellknown_flathub_verified() -> &'static str {
    "a29f43db-bd4e-40cb-b121-2899c4d70634\n"
}

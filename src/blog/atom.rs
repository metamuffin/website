use super::{
    helper::{get_articles, ArticleMeta},
    rocket_uri_macro_r_blog_article, rocket_uri_macro_r_blog_index, ARTICLE_ROOT,
};
use crate::{error::MyResult, uri};
use rocket::get;
use std::{path::PathBuf, str::FromStr};

#[get("/blog/feed.atom")]
pub async fn r_blog_atom() -> MyResult<String> {
    let entries = get_articles(&PathBuf::from_str(ARTICLE_ROOT).unwrap())
        .await?
        .iter()
        .map(
            |ArticleMeta {
                 title,
                 date,
                 canonical_name,
                 ..
             }| {
                let title = horrible_escape_function(title);
                let datetime = iso8601::DateTime {
                    date: date.clone(),
                    time: iso8601::Time::default(),
                };
                let href = uri!(r_blog_article(canonical_name));
                format!(
                    r#"
    <entry>
        <title>{title}</title>
        <link href="{href}" />
        <id>tag:metamuffin.org,{date},{canonical_name}</id>
        <published>{datetime}</published>
        <summary>N/A</summary>
        <author>
            <name>metamuffin</name>
            <email>metamuffin@disroot.org</email>
        </author>
    </entry>"#
                )
            },
        )
        .collect::<Vec<_>>();

    let feed_url = uri!(r_blog_atom());
    let index_url = uri!(r_blog_index());
    let now = chrono::Utc::now().to_rfc3339();

    Ok(format!(
        r#"<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
	<title>metamuffin's blog</title>
	<subtitle>where they post pointless stuff</subtitle>
	<link href="{feed_url}" rel="self" />
	<link href="{index_url}" />
	<id>urn:uuid:3cf2b704-3d94-4f1f-b194-42798ab5b47c</id>
	<updated>{now}</updated>
    <author>
        <name>metamuffin</name>
        <email>metamuffin@disroot.org</email>
    </author>
    {}
</feed>
    "#,
        entries.join("\n")
    ))
}

pub fn horrible_escape_function(text: &str) -> String {
    text.replace("&", "&amp;")
        .replace("<", "&lt;")
        .replace(">", "&gt;")
        .replace("'", "&#8217;")
        .replace("\"", "&quot;")
}

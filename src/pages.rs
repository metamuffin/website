use crate::uri;
use crate::{
    helper::get_articles,
    layout::{DynScaffold, Scaffold},
    rocket_uri_macro_r_blog_article, ARTICLE_ROOT,
};
use chrono::{NaiveTime, Utc};
use rocket::{get, http::ContentType, response::Redirect};
use std::{path::PathBuf, str::FromStr};

#[get("/")]
pub fn r_root() -> Redirect {
    Redirect::to(rocket::uri!(r_about()))
}

#[get("/hello")]
pub fn r_hello() -> &'static str {
    "Hello World!"
}

#[get("/about")]
pub async fn r_about<'a>() -> DynScaffold<'a> {
    let articles = get_articles(&PathBuf::from_str(ARTICLE_ROOT).unwrap())
        .await
        .unwrap_or_default();
    let mut newest = articles.first().cloned();
    if let Some(n) = &newest {
        if n.date
            .into_naive()
            .unwrap()
            .and_time(NaiveTime::default())
            .and_utc()
            .signed_duration_since(Utc::now())
            .num_days()
            < -8
        {
            newest = None;
        }
    }
    Scaffold {
        title: "about".to_string(),
        content: markup::new! {
            @if let Some(a) = &newest {
                .featured {
                    p { "Read the newest blog post: " a[href=uri!(r_blog_article(&a.canonical_name))] { @a.title } }
                }
            }
            p {
                "Hi. I am a normal person from planet earth. "
                "I enjoy starting projects and never finishing them. "
                "I do not know what to write here. "
                "I am also supporting the free software movement by writing exclusively free software in my spare time. "
            }
            h2 { "current interests" }
            ul {
                li { "development of a general-purpose systems programming language" }
                li { "development of multimedia processing and streaming applications" }
                li { "replacing existing software with rust rewrites and rocket emotes" }
                li { "stuff" }
            }
            h2 { "links" }
            p { "some of my projects if you are here because of boredom: " }
            ul {
                li { a[href="https://meet.metamuffin.org"] { "keks-meet" } }
                li { a[href="https://s.metamuffin.org/projects/weakpoint/"] { "weakpoint" } }
                li { a[href="https://meet.metamuffin.org"] { "keks-meet" } }
            }

        },
    }
}

#[get("/contact")]
pub fn r_contact() -> DynScaffold<'static> {
    Scaffold {
        title: "contact".to_string(),
        content: markup::new! {
            p { "You can reach out to me in a bunch of ways. I am also generally looking for nice software ideas to implement." }
            ul {
                li { "matrix: " a[href="https://matrix.to/#/@metamuffin:metamuffin.org"]{"@metamuffin:metamuffin.org"} }
                li { "fedi: " a[href="https://social.metamuffin.org/@metamuffin"]{"@metamuffin@social.metamuffin.org"} }
                li { "electronic mail: " a[href="mailto:metamuffin@disroot.org"]{"metamuffin@disroot.org"} }
                li { "telepathy: come on, just try hard enough" }
            }
        },
    }
}

#[get("/stuff")]
pub fn r_stuff() -> DynScaffold<'static> {
    Scaffold {
        title: "stuff".to_string(),
        content: markup::new! {
            h1 { "Stuff" }
            p { "I use arch btw." }
            p { "The server uses arch btw." }
            p { "My raspberry pi uses arch btw." }
            p { "My router uses arch btw." }
            p { "One of my smartphones uses arch btw." }
            h2 { "Funny People" }
            ul {
                li { a[href="https://potatoxel.org"] { "Potatoxel" } }
                li { a[href="https://pixificial.xyz"] { "Pixificial" } }
                li { a[href="https://rustystriker.dev"] { "Rustystriker" } }
                li { a[href="https://bgfxc4.de"] { "bgfxc4" } }
            }
        },
    }
}

#[get("/favicon.ico")]
pub fn r_favicon() {}

#[get("/key.asc")]
pub fn r_pgp_key() -> &'static str {
    include_str!("../assets/key.asc")
}

#[get("/style.css")]
pub fn r_style() -> (ContentType, &'static str) {
    (ContentType::CSS, include_str!("../assets/style.css"))
}
